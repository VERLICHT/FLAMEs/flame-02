from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .processinterview import * 
from django import forms
import django.conf as settings

class UploadFileForm(forms.Form):
    of = forms.FileField()


def file2lines(f):
    #returns a generator of decoded lines
    return (line.strip() for line in
            f.decode("utf-8").split("\n"))


def home(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            generator = request.FILES["of"].chunks(500000)
            lines = list(file2lines(next(generator)))
            request.session['_lines'] = lines
            for line in lines[240:260]:
                print(line)
            # errors = processLines(lines)
            # return result(request, errors)
            return redirect('result')
    else:
        form = UploadFileForm()
    return render(request, "validator/home.html", {"form": form, "title":"Upload a file"})

def test(request):
    print(dir(settings))
    with open(settings.settings.TEST_FILE, encoding="utf-8") as f:
        asdflines = f.readlines()
        errors = processLines(asdflines)
        return result(request, errors)

def result(request, errors=None):
    if '_lines' in request.session:
        lines = request.session.get('_lines')
        errors = processLines(lines)
            
    if errors is None:
        context = {'title':'Geen fouten gevonden!'}
    else:
        context = {'title': 'De volgende fouten zijn gevonden'}
        context['errors'] = errors
    return render(request, 'validator/result.html', context)
