import sys
from .classes import *

def processLines(lines):
    lines = [line for line in lines if line]
    error1 = UnmatchedError(lines)
    error2 = DistanceError(lines)
    error3 = CamelCaseError(lines)

    return error1, error2, error3

if __name__ == "__main__":
    fileName = sys.argv[1]
    fileHandle = open(fileName, 'r')
    lines = fileHandle.readlines()
    processLines(lines)





























