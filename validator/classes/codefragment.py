from django.utils.html import escape 

class Codefragment(object):
    def __init__(self, content, fragmentType):
        self.type= fragmentType
        self.content = escape(content.strip())

    def __len__(self):
        return len(self.content)

    def __str__(self):
        return f"{self.content}"
