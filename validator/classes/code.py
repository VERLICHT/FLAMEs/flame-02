from . import Codefragment

class Code(object):
    def __init__(self, *fragments):
        self.start, self.eind, self.type = [Codefragment(*f) for f in
                                            zip(fragments, ['start',
                                                            'eind',
                                                            'type' ])]

    def __iter__(self):
        return (x for x in [self.start, self.eind, self.type])

    def __str__(self):
        return "{self.start}, {self.eind}, {self.type}"
