import re
from . import Code

class Utterance(object):
    def __init__(self, lineNumber, line):
        self.uid, self.statement, self.codes = self.parseline(line)
        self.lineNumber = lineNumber


    def parseline(self, line, lineNumber=None):
        #remove whitespace
        line = " ".join(line.split())
        splitLine = [x.strip() for x in re.split("\]\]|\[\[", line) if x]


        if len(splitLine) == 1: #only a uid present
            return splitLine, "", []
        uid, statement, *codes = splitLine

        uid = re.split("=", uid)[-1]

        if not line.endswith("]]"): #UnmatchedError should catch this first!
            codes = codes[:-1]

        codes = [code for code in codes if code and not code.isspace()]

        if codes:
            codes = [Code(*re.split("\-\>|\|\||\|\>", code)) for code in codes]
            return uid, statement, codes

        return uid, statement, []


    def fragments(self):
        #returns for each fragment a tuple: (lineNumber, uid, fragmentContent)
        ret = []
        for code in self:
            for fragment in code:
                ret.append((self.lineNumber, self.uid, fragment.content))
        return ret


    def __iter__(self):
        return (code for code in self.codes)

