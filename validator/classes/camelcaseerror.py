import pandas as pd
import itertools as itt
import Levenshtein
import numpy as np
import re
import os
import codecs

from . import Error
from . import Utterance

#TODO: PeuterOpvang herkennen als fout (moet Peuteropvang zijn!)

class CamelCaseError(Error):
    def __init__(self, lines):
        self.common_words = ['causal', 'structural', 'causal_pos', 'causal_neg', 'assoc']
        self.all_dutch_words = self.dutch_words()
        utterances = (Utterance(i + 1, line) for i, line in enumerate(lines)
                      if not line.isspace())
        fragments = CamelCaseError.listFragments(utterances)
        self.wronglines = []


        #Used Multiprocessing.Pool, but no speedup!
        for fragment in fragments:
            if wrongline := self.check_fragment(fragment):
                self.wronglines.append(wrongline)

        retdf = pd.DataFrame(self.wronglines, columns = ['LineNumber',
                                                         'Fragment',
                                                         'Woord',
                                                    'Onbekend'] )

        #TODO if you do it wrong multiple times, it's (probably) right?
        retdf = retdf[~retdf.Woord.duplicated(keep=False)]

        self.header = """De volgende regels bevatten fouten in CamelCase of
        spelfouten:"""
        self.uitleg = """De codefragmenten worden gesplitst bij de hoofdletter
        en daarna worden de individuele woorden opgezocht het Nederlands
        woordenboek. Als een van de woorden niet voorkomt in het woordenboek,
        wordt de regel in de tabel gezet ter controle. Het programma is niet
        altijd correct: Zorgpeuter (correct) wordt herkend als fout, maar
        ZorgPeuter (niet correct) niet. Dit komt omdat het woord zorgpeuter
        niet in het wordenboek staat, maar wel een valide samentrekking is. Het
        is ondoenlijk om alle valide samentrekkingen te detecteren."""

        if len(retdf) == 0:
            self.html = """<div style="color:green">Geen codefragmenten
            gevonden met spelfouten of foute CamelCase.</div>"""
        else:
            self.html = retdf.to_html(index=False, escape=True)

    def check_fragment(self, fragment):
        fragment = list(fragment)
        camel = fragment[2]
        fragment[2] = self.split_at_capital(fragment[2])
        lineNumber = fragment[0]

        for word in fragment[2]:
            origWord = word
            word = word.lower()
            if not word or word in self.common_words:
                continue
            if not self.is_dutch_word(word):
               return [lineNumber, camel, origWord, "woord niet gevonden"]

        return None


    def listFragments(utterances):
            #list all fragments (uid, line#, content) for all utterances
            return [fragment for utt in utterances for fragment in utt.fragments()]

    def is_dutch_word(self, word) -> bool:
        if word in self.all_dutch_words[word[0]].values:
            return True
        if word in self.all_dutch_words['rest'].values:
            return True

        newword = word[:-1] + "'" + word[-1]
        if newword in self.all_dutch_words[word[0]].values:
            return True

        if newword in self.all_dutch_words['rest'].values:
            return True

        return False

    def dutch_words(self) -> pd.DataFrame:
        #list all fragments (uid, line#, content) for all utterances
        columns = {}
        columns['rest'] = []
        for letter in "abcdefghijklmnopqrstuvwxyz":
            columns[letter] = []

        with codecs.open("/usr/share/dict/dutch", encoding='utf-8') as f:
            for line in f.readlines():
                line = line.lower().strip()
                if line[0] in columns.keys():
                    columns[line[0]].append(line)
                else:
                    columns['rest'].append(line)
        
        df =  pd.DataFrame(dict([(k, pd.Series(v)) for k, v in columns.items()]))
        print("andere" in df['a'])
        return df

    def split_at_capital(self, line) -> list:
        split = re.split('([A-Z])', line)
        ret = []
        ret.append(split[0])
        for i in range(1, len(split), 2):
            ret.append(split[i] + split[i + 1])
        newRet = []
        newWord = ""
        for word in ret:
            if len(word) == 1:
                newWord += word
            else:
                if newWord:
                    newRet.append(newWord)
                    newWord = ""
                newRet.append(word)
        return newRet


if __name__ == '__main__':
    with open('../test.txt') as f:
        lines = f.readlines()
        error = CamelCaseError(lines)

