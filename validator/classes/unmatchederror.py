import pandas as pd
from . import Error
from django.utils.html import escape

class UnmatchedError(Error):
    def __init__(self, lines):
        self.header = "De volgende regels hebben ongebalanceerde haakjes:"
        df = self.unmatched_lines(lines)
        if len(df) == 0:
            self.html = """<div style="color:green">Geen ongebalanceerde
            haakjes gevonden! (maar ze kunnen er wel zijn!)</div>"""
        else:
            self.html = df.to_html(index=False, escape=False)


    def unmatched_lines(self, lines) -> pd.DataFrame:
        unmatched = []
        for i, line in enumerate(lines):
            if line:
                lineNotMatched = self.line_is_unmatched(line)
                if lineNotMatched:
                    unmatched.append([i+1, lineNotMatched])

        return pd.DataFrame(unmatched, columns=["Regel#","Inhoud"])

    
    def line_is_unmatched(self, line):
        count, lastOpen, lastClosed = 0, 0, 0
        for idx, c in enumerate(line):
            if c == '[':
                count += 1
                lastOpen = idx
            elif c == ']':
                count -= 1
                lastClosed = idx
            if count < 0:
                break

        highlight = lastOpen if count > 0 else lastClosed
        line = escape(line) 

        return self.highlightCharInLine(line, highlight) if count else False


    def highlightCharInLine(self, s, pos):
        return f"{s[:pos]} <mark>{s[pos]}</mark>{s[pos+1:]}"

