import pandas as pd
import itertools as itt
import Levenshtein
import numpy as np

from . import Error
from . import Utterance

class DistanceError(Error):
    def __init__(self, lines):
        utterances = (Utterance(i + 1, line) for i, line in enumerate(lines)
                      if not line.isspace())

        fragments = DistanceError.listFragments(utterances)
        df = DistanceError.fragmentCombinations(fragments)
        df['distance'] = DistanceError.distance(df['FRAGMENT1'], df['FRAGMENT2'])
        df = df[(df['distance'] < 0.15) & df['distance'] > 0]
        df = df[DistanceError.filterCommonWords(df['FRAGMENT1'], df['FRAGMENT2'])]
        df = df.drop_duplicates(subset=['FRAGMENT1', 'FRAGMENT2'], keep='last')

        self.header = "De volgende regels bevatting code fragmenten die erg op andere lijken:"
        if len(df) == 0:
            self.html = """<div style="color:green">Geen codefragmenten
            gevonden die erg op andere lijken.</div>"""
        else:
            self.html = df.to_html(index=False, 
                                   escape=False, 
                                   table_id="sortTable",
                                   classes=["sortTable"])

    def listFragments(utterances):
        #list all fragments (uid, line#, content) for all utterances
        return [fragment for utt in utterances for fragment in utt.fragments()]


    def fragmentCombinations(fragments) -> pd.DataFrame:
        indices =  list(itt.combinations(range(len(fragments)),2))
        df = pd.DataFrame([fragments[index1] + fragments[index2] for
                           index1,index2 in indices],
                           columns=["Regel#1","UID1","FRAGMENT1",
                                    "Regel#2","UID2","FRAGMENT2"])
        return df


    @np.vectorize
    def distance(s1, s2):
        return Levenshtein.distance(s1, s2) / (len(s1) + len(s2))


    @np.vectorize
    def filterCommonWords(word1, word2):
        #TRUE if either w1 or w2 in in common
        common = ['structural', 'causal_neg', 'causal_neg']
        return set(common).intersection({word1, word2}) == set()
