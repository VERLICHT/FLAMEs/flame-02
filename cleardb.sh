#! /bin/bash

rm db.sqlite3
rm -rf validator/migrations/*
rm -rf validator/__pycache__/*
rm -rf config/__pycache__/*
source ~/.virtualenvs/validator/bin/activate
./manage.py makemigrations
./manage.py migrate --run-syncdb
