#! /bin/bash

rm db.sqlite3
rm -rf sunscreen/screen/migrations/*
rm -rf sunscreen/screen/__pycache__/*
rm -rf config/__pycache__/*
source ~/.virtualenvs/sunscreen/bin/activate
./manage.py makemigrations
./manage.py migrate --run-syncdb
DJANGO_SUPERUSER_USERNAME=admin \
DJANGO_SUPERUSER_PASSWORD=pwd45 \
DJANGO_SUPERUSER_EMAIL="hp3@ou.nl" \
./manage.py createsuperuser --noinput
